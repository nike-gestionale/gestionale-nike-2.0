package it.nike.gestnike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestnikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestnikeApplication.class, args);
	}

}
